#!/bin/sh
#
# Creates an empty bare repos in the repos folder
#
# Usage: sh new_repos.sh reposname

stagitdir="/var/www/git.bleu255.com"
reposdir="${stagitdir}/repos"
server="borok"
reposurl="https://git.bleu255.com/repos" # no trailing slash!
owner="danse macabre"

# safety nets
if [ "$(pwd)" != "${stagitdir}" ]
then
    echo "run me from ${stagitdir}"
    exit 1
fi

if [ "$1" = "" ] || [ "$2" = "" ]
then
    echo 'Usage: sh new_repos.sh reposname "one line about the repos"'
    exit 1
fi

if [ -e ${reposdir}/$1.git ]
then
    echo "$1 repos already exists!"
    exit 1
fi

# git init
git init --bare ${reposdir}/$1.git

# create the post-receive hook
ln -s ${stagitdir}/update_single.sh ${reposdir}/$1.git/hooks/post-receive

# set repos info
echo ${owner} > ${reposdir}/$1.git/owner
echo $2 > ${reposdir}/$1.git/description
echo "${reposurl}/$1.git" > ${reposdir}/$1.git/url

# bye
echo "ALL GOOD!\npush URL: ${server}:${reposdir}/$1.git"

