#!/bin/sh
# delete all the static rendering of the repos
# needed when stagit is updated with new features

reset_repos()
{
for REPOS in $(ls repos)
do
  STATIC=$(basename -s .git ${REPOS})
  echo ${STATIC}
	rm -rf ${STATIC}
done
}

while true; do
	read -p "are you sure you know what you're doing (y/n)? " yn
		case $yn in
			[Yy]* ) reset_repos; break;;
			[Nn]* ) exit;;
			* ) echo "what?";;
		esac
done

