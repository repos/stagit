#!/bin/sh
# - Makes index for repositories in a single directory.
# - Makes static pages for each repository directory.
#
# Usage:
# - mkdir -p htmldir && cd htmldir
# - sh example.sh

# path must be absolute.
reposdir="/var/www/git.bleu255.com/repos"
curdir=$(pwd)

# make index.
stagit-index "${reposdir}/"*/ > "${curdir}/index.html"

# make files per repo.
for dir in "${reposdir}/"*/; do
	# strip .git suffix.
	r=$(basename "${dir}")
	d=$(basename "${dir}" ".git")
	printf "%s... " "${d}"

	mkdir -p "${curdir}/${d}"
	cd "${curdir}/${d}"
	stagit -c ".cache" "${reposdir}/${r}"

	# symlinks
	ln -sf log.html index.html
	ln -sf ../style.css style.css
	ln -sf ../logo.png logo.png
	ln -sf ../favicon.png favicon.png

	printf "done\n"
done
