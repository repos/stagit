#!/bin/sh
# - Makes static pages for current directory.
#
# Usage:
# - cd repos/something
# - ../../update_single.sh

# Are we in a Git repos?
if [ ! -e HEAD ]  # Ahem... :)
then
	echo "You don't want to run this here..."
	exit 1
fi

# first we update auxiliary info to serve repos over HTTP
git update-server-info

# path must be absolute.
reposdir="/var/www/git.bleu255.com/repos/"
stagitdir="/var/www/git.bleu255.com/"
curdir=$(pwd)

# strip .git suffix.
r=$(basename "${curdir}")
d=$(basename "${curdir}" ".git")
printf "%s... " "${d}"


mkdir -p "${stagitdir}/${d}"
cd "${stagitdir}/${d}"
stagit -c ".cache" "${reposdir}/${r}"

# symlinks
ln -sf log.html index.html
ln -sf ../style.css style.css
ln -sf ../logo.png logo.png
ln -sf ../favicon.png favicon.png

# force update index in case first push on brand new repos
stagit-index "${reposdir}/"*/ > "${stagitdir}/index.html"

printf "done\n"
